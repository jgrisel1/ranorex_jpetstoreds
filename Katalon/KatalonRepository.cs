﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Repository;
using Ranorex.Core.Testing;

namespace Katalon
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    /// The class representing the KatalonRepository element repository.
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
    [RepositoryFolder("1509e6ec-fc13-4906-891e-e2a477d8a30d")]
    public partial class KatalonRepository : RepoGenBaseFolder
    {
        static KatalonRepository instance = new KatalonRepository();
        KatalonRepositoryFolders.ApplicationUnderTestAppFolder _applicationundertest;
        KatalonRepositoryFolders.CURAHealthcareServiceGoogleChromeAppFolder _curahealthcareservicegooglechrome;

        /// <summary>
        /// Gets the singleton class instance representing the KatalonRepository element repository.
        /// </summary>
        [RepositoryFolder("1509e6ec-fc13-4906-891e-e2a477d8a30d")]
        public static KatalonRepository Instance
        {
            get { return instance; }
        }

        /// <summary>
        /// Repository class constructor.
        /// </summary>
        public KatalonRepository() 
            : base("KatalonRepository", "/", null, 0, false, "1509e6ec-fc13-4906-891e-e2a477d8a30d", ".\\RepositoryImages\\KatalonRepository1509e6ec.rximgres")
        {
            _applicationundertest = new KatalonRepositoryFolders.ApplicationUnderTestAppFolder(this);
            _curahealthcareservicegooglechrome = new KatalonRepositoryFolders.CURAHealthcareServiceGoogleChromeAppFolder(this);
        }

#region Variables

#endregion

        /// <summary>
        /// The Self item info.
        /// </summary>
        [RepositoryItemInfo("1509e6ec-fc13-4906-891e-e2a477d8a30d")]
        public virtual RepoItemInfo SelfInfo
        {
            get
            {
                return _selfInfo;
            }
        }

        /// <summary>
        /// The ApplicationUnderTest folder.
        /// </summary>
        [RepositoryFolder("8f639ead-8658-4fd0-9439-32b7a5c14d75")]
        public virtual KatalonRepositoryFolders.ApplicationUnderTestAppFolder ApplicationUnderTest
        {
            get { return _applicationundertest; }
        }

        /// <summary>
        /// The CURAHealthcareServiceGoogleChrome folder.
        /// </summary>
        [RepositoryFolder("cbbfbca5-d7a2-47f3-8f9e-62edbfa3f186")]
        public virtual KatalonRepositoryFolders.CURAHealthcareServiceGoogleChromeAppFolder CURAHealthcareServiceGoogleChrome
        {
            get { return _curahealthcareservicegooglechrome; }
        }
    }

    /// <summary>
    /// Inner folder classes.
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
    public partial class KatalonRepositoryFolders
    {
        /// <summary>
        /// The ApplicationUnderTestAppFolder folder.
        /// </summary>
        [RepositoryFolder("8f639ead-8658-4fd0-9439-32b7a5c14d75")]
        public partial class ApplicationUnderTestAppFolder : RepoGenBaseFolder
        {

            /// <summary>
            /// Creates a new ApplicationUnderTest  folder.
            /// </summary>
            public ApplicationUnderTestAppFolder(RepoGenBaseFolder parentFolder) :
                    base("ApplicationUnderTest", "/dom[@domain='katalon-demo-cura.herokuapp.com']", parentFolder, 30000, null, false, "8f639ead-8658-4fd0-9439-32b7a5c14d75", "")
            {
            }

            /// <summary>
            /// The Self item.
            /// </summary>
            [RepositoryItem("8f639ead-8658-4fd0-9439-32b7a5c14d75")]
            public virtual Ranorex.WebDocument Self
            {
                get
                {
                    return _selfInfo.CreateAdapter<Ranorex.WebDocument>(true);
                }
            }

            /// <summary>
            /// The Self item info.
            /// </summary>
            [RepositoryItemInfo("8f639ead-8658-4fd0-9439-32b7a5c14d75")]
            public virtual RepoItemInfo SelfInfo
            {
                get
                {
                    return _selfInfo;
                }
            }
        }

        /// <summary>
        /// The CURAHealthcareServiceGoogleChromeAppFolder folder.
        /// </summary>
        [RepositoryFolder("cbbfbca5-d7a2-47f3-8f9e-62edbfa3f186")]
        public partial class CURAHealthcareServiceGoogleChromeAppFolder : RepoGenBaseFolder
        {
            RepoItemInfo _makeappointmentInfo;
            RepoItemInfo _noneInfo;
            RepoItemInfo _passwordInfo;
            RepoItemInfo _loginInfo;
            RepoItemInfo _facilityInfo;
            RepoItemInfo _medicaidInfo;
            RepoItemInfo _sometextInfo;
            RepoItemInfo _commentInfo;
            RepoItemInfo _bookappointmentInfo;
            RepoItemInfo _gotohomepageInfo;
            RepoItemInfo _somelinkInfo;
            RepoItemInfo _logoutInfo;

            /// <summary>
            /// Creates a new CURAHealthcareServiceGoogleChrome  folder.
            /// </summary>
            public CURAHealthcareServiceGoogleChromeAppFolder(RepoGenBaseFolder parentFolder) :
                    base("CURAHealthcareServiceGoogleChrome", "/form[@title>'CURA Healthcare Service -']", parentFolder, 30000, null, true, "cbbfbca5-d7a2-47f3-8f9e-62edbfa3f186", "")
            {
                _makeappointmentInfo = new RepoItemInfo(this, "MakeAppointment", ".//text[@accessiblename='Make Appointment']", ".//text[@accessiblename='Make Appointment']", 30000, null, "1acab73a-6068-4f59-9426-be6d3ab82874");
                _noneInfo = new RepoItemInfo(this, "None", "?//text[@accessiblename='Username' and @accessiblerole='Text']", ".//text[@accessiblename='Username' and @accessiblerole='Text']", 30000, null, "f0797111-56eb-41e1-a8d4-d3e6aadd9675");
                _passwordInfo = new RepoItemInfo(this, "Password", "?//text[@accessiblename='Password' and @accessiblerole='Text']", ".//text[@accessiblename='Password' and @accessiblerole='Text']", 30000, null, "fcc8928d-129d-46a9-b88a-57d54bb70499");
                _loginInfo = new RepoItemInfo(this, "Login", ".//button[@accessiblename='Login']", ".//button[@accessiblename='Login']", 30000, null, "7139dfaa-c67d-4a24-a2ab-304ded03d102");
                _facilityInfo = new RepoItemInfo(this, "Facility", ".//combobox[@accessiblename='Facility']", ".//combobox[@accessiblename='Facility']", 30000, null, "013409d9-cbfd-43b7-b0bd-16aae29c5b98");
                _medicaidInfo = new RepoItemInfo(this, "Medicaid", ".//radiobutton[@accessiblename='Medicaid']", ".//radiobutton[@accessiblename='Medicaid']", 30000, null, "177ece6e-af16-44a9-a737-48b3c155d896");
                _sometextInfo = new RepoItemInfo(this, "SomeText", ".//text[@accessiblename='']", ".//text[@accessiblename='']", 30000, null, "3f07bdcb-2822-442a-a90a-386b59102b1e");
                _commentInfo = new RepoItemInfo(this, "Comment", ".//text[@accessiblename='Comment' and @accessiblerole='Text']", ".//text[@accessiblename='Comment' and @accessiblerole='Text']", 30000, null, "d46ffe35-39d2-4896-9942-401b3bfa561c");
                _bookappointmentInfo = new RepoItemInfo(this, "BookAppointment", ".//button[@accessiblename='Book Appointment']", ".//button[@accessiblename='Book Appointment']", 30000, null, "001d9717-262a-4053-9474-ec0d8d6b11ea");
                _gotohomepageInfo = new RepoItemInfo(this, "GoToHomepage", ".//text[@accessiblename='Go to Homepage']", ".//text[@accessiblename='Go to Homepage']", 30000, null, "b311986d-1b5d-4da9-a030-46bb4b4f8223");
                _somelinkInfo = new RepoItemInfo(this, "SomeLink", ".//link[@accessiblename='']", ".//link[@accessiblename='']", 30000, null, "6323b8c3-5b0b-4ac9-9bc8-e7979a87a278");
                _logoutInfo = new RepoItemInfo(this, "Logout", ".//text[@accessiblename='Logout']", ".//text[@accessiblename='Logout']", 30000, null, "79f03d49-a11e-4523-b0b4-c0d7772392a3");
            }

            /// <summary>
            /// The Self item.
            /// </summary>
            [RepositoryItem("cbbfbca5-d7a2-47f3-8f9e-62edbfa3f186")]
            public virtual Ranorex.Form Self
            {
                get
                {
                    return _selfInfo.CreateAdapter<Ranorex.Form>(true);
                }
            }

            /// <summary>
            /// The Self item info.
            /// </summary>
            [RepositoryItemInfo("cbbfbca5-d7a2-47f3-8f9e-62edbfa3f186")]
            public virtual RepoItemInfo SelfInfo
            {
                get
                {
                    return _selfInfo;
                }
            }

            /// <summary>
            /// The MakeAppointment item.
            /// </summary>
            [RepositoryItem("1acab73a-6068-4f59-9426-be6d3ab82874")]
            public virtual Ranorex.Text MakeAppointment
            {
                get
                {
                    return _makeappointmentInfo.CreateAdapter<Ranorex.Text>(true);
                }
            }

            /// <summary>
            /// The MakeAppointment item info.
            /// </summary>
            [RepositoryItemInfo("1acab73a-6068-4f59-9426-be6d3ab82874")]
            public virtual RepoItemInfo MakeAppointmentInfo
            {
                get
                {
                    return _makeappointmentInfo;
                }
            }

            /// <summary>
            /// The None item.
            /// </summary>
            [RepositoryItem("f0797111-56eb-41e1-a8d4-d3e6aadd9675")]
            public virtual Ranorex.Text None
            {
                get
                {
                    return _noneInfo.CreateAdapter<Ranorex.Text>(true);
                }
            }

            /// <summary>
            /// The None item info.
            /// </summary>
            [RepositoryItemInfo("f0797111-56eb-41e1-a8d4-d3e6aadd9675")]
            public virtual RepoItemInfo NoneInfo
            {
                get
                {
                    return _noneInfo;
                }
            }

            /// <summary>
            /// The Password item.
            /// </summary>
            [RepositoryItem("fcc8928d-129d-46a9-b88a-57d54bb70499")]
            public virtual Ranorex.Text Password
            {
                get
                {
                    return _passwordInfo.CreateAdapter<Ranorex.Text>(true);
                }
            }

            /// <summary>
            /// The Password item info.
            /// </summary>
            [RepositoryItemInfo("fcc8928d-129d-46a9-b88a-57d54bb70499")]
            public virtual RepoItemInfo PasswordInfo
            {
                get
                {
                    return _passwordInfo;
                }
            }

            /// <summary>
            /// The Login item.
            /// </summary>
            [RepositoryItem("7139dfaa-c67d-4a24-a2ab-304ded03d102")]
            public virtual Ranorex.Button Login
            {
                get
                {
                    return _loginInfo.CreateAdapter<Ranorex.Button>(true);
                }
            }

            /// <summary>
            /// The Login item info.
            /// </summary>
            [RepositoryItemInfo("7139dfaa-c67d-4a24-a2ab-304ded03d102")]
            public virtual RepoItemInfo LoginInfo
            {
                get
                {
                    return _loginInfo;
                }
            }

            /// <summary>
            /// The Facility item.
            /// </summary>
            [RepositoryItem("013409d9-cbfd-43b7-b0bd-16aae29c5b98")]
            public virtual Ranorex.ComboBox Facility
            {
                get
                {
                    return _facilityInfo.CreateAdapter<Ranorex.ComboBox>(true);
                }
            }

            /// <summary>
            /// The Facility item info.
            /// </summary>
            [RepositoryItemInfo("013409d9-cbfd-43b7-b0bd-16aae29c5b98")]
            public virtual RepoItemInfo FacilityInfo
            {
                get
                {
                    return _facilityInfo;
                }
            }

            /// <summary>
            /// The Medicaid item.
            /// </summary>
            [RepositoryItem("177ece6e-af16-44a9-a737-48b3c155d896")]
            public virtual Ranorex.RadioButton Medicaid
            {
                get
                {
                    return _medicaidInfo.CreateAdapter<Ranorex.RadioButton>(true);
                }
            }

            /// <summary>
            /// The Medicaid item info.
            /// </summary>
            [RepositoryItemInfo("177ece6e-af16-44a9-a737-48b3c155d896")]
            public virtual RepoItemInfo MedicaidInfo
            {
                get
                {
                    return _medicaidInfo;
                }
            }

            /// <summary>
            /// The SomeText item.
            /// </summary>
            [RepositoryItem("3f07bdcb-2822-442a-a90a-386b59102b1e")]
            public virtual Ranorex.Text SomeText
            {
                get
                {
                    return _sometextInfo.CreateAdapter<Ranorex.Text>(true);
                }
            }

            /// <summary>
            /// The SomeText item info.
            /// </summary>
            [RepositoryItemInfo("3f07bdcb-2822-442a-a90a-386b59102b1e")]
            public virtual RepoItemInfo SomeTextInfo
            {
                get
                {
                    return _sometextInfo;
                }
            }

            /// <summary>
            /// The Comment item.
            /// </summary>
            [RepositoryItem("d46ffe35-39d2-4896-9942-401b3bfa561c")]
            public virtual Ranorex.Text Comment
            {
                get
                {
                    return _commentInfo.CreateAdapter<Ranorex.Text>(true);
                }
            }

            /// <summary>
            /// The Comment item info.
            /// </summary>
            [RepositoryItemInfo("d46ffe35-39d2-4896-9942-401b3bfa561c")]
            public virtual RepoItemInfo CommentInfo
            {
                get
                {
                    return _commentInfo;
                }
            }

            /// <summary>
            /// The BookAppointment item.
            /// </summary>
            [RepositoryItem("001d9717-262a-4053-9474-ec0d8d6b11ea")]
            public virtual Ranorex.Button BookAppointment
            {
                get
                {
                    return _bookappointmentInfo.CreateAdapter<Ranorex.Button>(true);
                }
            }

            /// <summary>
            /// The BookAppointment item info.
            /// </summary>
            [RepositoryItemInfo("001d9717-262a-4053-9474-ec0d8d6b11ea")]
            public virtual RepoItemInfo BookAppointmentInfo
            {
                get
                {
                    return _bookappointmentInfo;
                }
            }

            /// <summary>
            /// The GoToHomepage item.
            /// </summary>
            [RepositoryItem("b311986d-1b5d-4da9-a030-46bb4b4f8223")]
            public virtual Ranorex.Text GoToHomepage
            {
                get
                {
                    return _gotohomepageInfo.CreateAdapter<Ranorex.Text>(true);
                }
            }

            /// <summary>
            /// The GoToHomepage item info.
            /// </summary>
            [RepositoryItemInfo("b311986d-1b5d-4da9-a030-46bb4b4f8223")]
            public virtual RepoItemInfo GoToHomepageInfo
            {
                get
                {
                    return _gotohomepageInfo;
                }
            }

            /// <summary>
            /// The SomeLink item.
            /// </summary>
            [RepositoryItem("6323b8c3-5b0b-4ac9-9bc8-e7979a87a278")]
            public virtual Ranorex.Link SomeLink
            {
                get
                {
                    return _somelinkInfo.CreateAdapter<Ranorex.Link>(true);
                }
            }

            /// <summary>
            /// The SomeLink item info.
            /// </summary>
            [RepositoryItemInfo("6323b8c3-5b0b-4ac9-9bc8-e7979a87a278")]
            public virtual RepoItemInfo SomeLinkInfo
            {
                get
                {
                    return _somelinkInfo;
                }
            }

            /// <summary>
            /// The Logout item.
            /// </summary>
            [RepositoryItem("79f03d49-a11e-4523-b0b4-c0d7772392a3")]
            public virtual Ranorex.Text Logout
            {
                get
                {
                    return _logoutInfo.CreateAdapter<Ranorex.Text>(true);
                }
            }

            /// <summary>
            /// The Logout item info.
            /// </summary>
            [RepositoryItemInfo("79f03d49-a11e-4523-b0b4-c0d7772392a3")]
            public virtual RepoItemInfo LogoutInfo
            {
                get
                {
                    return _logoutInfo;
                }
            }
        }

    }
#pragma warning restore 0436
}
